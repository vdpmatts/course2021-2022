# 5. Get the number of inhabitants of Santiago de Compostela and Arzua

prefix ex:<http://GP-onto.fi.upm.es/exercise2#>
select ?o 
where {
	{ex:Santiago_de_Compostela ex:hasInhabitantNumber ?o}
UNION
 {ex:Arzua ex:hasInhabitantNumber ?o}
}
