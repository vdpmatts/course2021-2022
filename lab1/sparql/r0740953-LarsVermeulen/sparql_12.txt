# 12. Describe the resource with rdfs:label "Madrid”

prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>
describe ?s 
where {
	?s rdfs:label "Madrid"
}