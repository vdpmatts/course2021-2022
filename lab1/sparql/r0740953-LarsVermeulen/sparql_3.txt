# 3. Get all instances of class City

prefix ex:<http://GP-onto.fi.upm.es/exercise2#>
select ?s 
where {
	?s rdf:type ex:City
}