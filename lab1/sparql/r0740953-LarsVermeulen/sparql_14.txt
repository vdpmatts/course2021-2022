# 14. Ask whether there is any instance of Town

prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix ex: <http://GP-onto.fi.upm.es/exercise2#>

ask { ?s rdf:type ex:Town. }