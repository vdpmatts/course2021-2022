# 2. Get all the subclasses of the class Establishment

PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix ex:<http://GP-onto.fi.upm.es/exercise2#>

select ?s 
where {
	?s rdfs:subClassOf ex:Establishment
}